package pl.alx.kurs.generyki;

public class Przyklad {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PudelkoInt pudelko1 = new PudelkoInt(45);
		PudelkoDouble pudelko2 = new PudelkoDouble(34.5);

		Pudelko<Integer> pudelko3 = new Pudelko<Integer>(21);
		Pudelko<Double> pudelkoD = new Pudelko<Double>(34.4);
		Pudelko<String> pudelkoS = new Pudelko<String>("ahdbkasda");
		DuzePudelko<Integer, Double, String> pudelkoDuze = new DuzePudelko<Integer, Double, String>();
		
	}

}
