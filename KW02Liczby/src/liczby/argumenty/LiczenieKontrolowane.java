package liczby.argumenty;

import java.util.Scanner;

public class LiczenieKontrolowane {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int x = sc.nextInt();
		int y = sc.nextInt();

		int wynik = 0;
		try {
			wynik = Math.multiplyExact(x, y);
		} catch (Exception e) {
			System.out.println("Blad wykonania");
		}

		System.out.println("Wynik: " + wynik);

		sc.close();

	}

}
