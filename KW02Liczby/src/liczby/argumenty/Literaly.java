package liczby.argumenty;

public class Literaly {

	public static void main(String[] args) {

		int liczba = 99;
		System.out.println("Liczba: " + liczba);
		liczba = 0123;   //Dodanie zero na pozcatku -> format osemkowy
		System.out.println("Liczba: " + liczba);
		//liczba =099 nie mozna bo w osemkowym nie ma cyfry 9
		
		liczba = 0x123f; //szesnastkowy
		System.out.println("Liczba: " + liczba);
		liczba = 0b01011; //dwojkowy
		System.out.println("Liczba: " + liczba);
		
		
		long duza = 1;
		duza =1L;
		
		
		float f1 = 3;
	//	f1 = 3.14; //nieprawidlowo, domyslnie double
		f1 = 3.14f; //prawidlowo (typ float dopisany po prawej stronie)
		
				
		double d1 = 2;
		d1 = 3.14;
		
		double wysokosc = 5;
		double podstawa =5;
		double wynik = podstawa*wysokosc/2;
		System.out.println("Wynik: "+wynik);
		
		
		
	}

}
