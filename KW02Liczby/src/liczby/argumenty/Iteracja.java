package liczby.argumenty;

public class Iteracja {

	public static void main(String[] args) {
		int liczba = 2;
		System.out.println("Liczba: " + liczba);
		liczba = liczba + 1;
		liczba +=1;
		System.out.println("liczba: "+liczba);
		
		liczba++;
		System.out.println("Liczba: "+liczba);
		++liczba;
		System.out.println("Liczba: "+liczba);
		
		
		System.out.println("=====================================");
		
		System.out.println("Liczba: "+ (++liczba));
		System.out.println("Liczba: " + (liczba++));
		System.out.println("Liczba: "+liczba);
		
		
		System.out.println("Liczba: "+ (--liczba));
		System.out.println("Liczba: "+ (liczba--));
		System.out.println("Liczba:"+liczba);
		
	}

}
