package liczby.argumenty;

import java.math.BigDecimal;

public class Ciekawostki {
	public static void main(String[] args) {
		
		System.out.println(5 * 1_000_000_000);
		System.out.println(5 * 1_000_000_000L);
		System.out.println(5L * 1000000000);
		BigDecimal bd = new BigDecimal("1.3");
		System.out.println(bd);
		
		BigDecimal mnoznik = new BigDecimal("3");
		BigDecimal wynik = bd.multiply(mnoznik);
		System.out.println(wynik);
		
		
		
	}

}
