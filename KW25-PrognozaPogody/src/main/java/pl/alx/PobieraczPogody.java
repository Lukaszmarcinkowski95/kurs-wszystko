/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.alx;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.alx.pogoda.model.Pogoda;

/**
 *
 * @author kurs
 */
public class PobieraczPogody {
    public static String URL = "http://api.openweathermap.org/data/2.5/weather?q=";
            public static String APPID = "&appid=a6e47c8fcee22c5f18c55fdbb507fe8a";
    
    public static double pobierzTemperature(String miasto){  
        Pogoda pogoda;
        try {
            String pobraneDane = readFromWeb(URL+miasto+APPID);
            Gson gson = new Gson();
            pogoda = gson.fromJson(pobraneDane, Pogoda.class);
            System.out.println(""+pobraneDane);
        } catch (IOException ex) {
//            Logger.getLogger(PobieraczPogody.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        
        
      return PrzeliczanieStopni.fahrNaCelc(pogoda.getMain().getTemp());  
    }
    public static String readFromWeb(String webURL) throws IOException {
        String data ="";
        URL url = new URL(webURL);
        InputStream is =  url.openStream();
        try( BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
                data+=line;
            }
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            throw new MalformedURLException("URL jest zjebany!!");
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new IOException();
        }
        return data;
    }
    
    
    
}
