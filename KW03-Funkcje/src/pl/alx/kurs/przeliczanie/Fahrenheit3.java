package pl.alx.kurs.przeliczanie;

public class Fahrenheit3 {

	public static void main(String[] args) {
		double[] tablica = {93, 154.78, 78, 109, 55.23, -21};
		int i = 1;
		
		
		
		for (double liczba : tablica) {
			double celc = PrzeliczanieJednostek.fahrNaCelc(liczba);
			System.out.println(i + ". " +liczba + "'F to " + String.format("%.2f", celc) + "'C");
			i++;
			
					
		}
	}

}
