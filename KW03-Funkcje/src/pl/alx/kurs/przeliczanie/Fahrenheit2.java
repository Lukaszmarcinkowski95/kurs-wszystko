package pl.alx.kurs.przeliczanie;

public class Fahrenheit2 {

	public static void main(String[] args) {

		double[] tablicaDoubli = { 23.5, 43.1, 11.5, 22.3 };

		System.out.println("Wielkosc tablicy: " + tablicaDoubli.length);
		System.out.println("Element 0: " + tablicaDoubli[0]);
		System.out.println("Element 1: " + tablicaDoubli[1]);
		System.out.println("Element 2: " + tablicaDoubli[2]);
		System.out.println("Element 3: " + tablicaDoubli[3]);

		for (double liczba : tablicaDoubli) {
			double fahr = PrzeliczanieJednostek.celcNaFahr(liczba);

			System.out.println("Liczba: " + liczba);
			System.out.println(liczba + "C to " + String.format("%.2f", fahr) + "T");

		}

	}

}
