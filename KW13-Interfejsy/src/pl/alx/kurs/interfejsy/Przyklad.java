package pl.alx.kurs.interfejsy;

public class Przyklad {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TelefonKomorkowy telefon1 = new TelefonKomorkowy();
		TelefonStacjonarny telefon2 = new TelefonStacjonarny();
		telefon1.zadzwon("+48 666444000");
		telefon2.zadzwon("+48798543122");
		
		
		//Nie powolamy interfejsu do zycia
		//ITelefon interfejs = new ITelefon();      <- nie da sie
		
		
		ITelefon telefonJakis = new TelefonKomorkowy();
		telefonJakis.zadzwon("112");
		telefon1.wymienBaterie();
		//Brak metody obiektu klasy TelefonKomorkowy. Obiekt dostepny przez interfejs
		//telefonJakis.wymienBaterie();  <- nie da sie bo telefonJakis byl tworzony w interfejsie a nie klasie
		
		if(telefonJakis instanceof TelefonKomorkowy) {
			TelefonKomorkowy telk = (TelefonKomorkowy) telefonJakis;
			telk.wymienBaterie();
			
			
		}
		IAparat aparat = telefon1;
		aparat.zrobZdjecie();
		//IAparat aparat2 = telefonJakis;
		if(telefonJakis instanceof IAparat) {
			IAparat aparat2 = (IAparat) telefonJakis;
			aparat2.zrobZdjecie();
			
		}
		
	}

}
