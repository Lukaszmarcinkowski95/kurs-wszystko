package Abstrakt1;

import java.util.ArrayList;
import java.util.List;

public class Run {

	public static void main(String[] args) {
		
		
		List<Figura> lista = new ArrayList<>();
		lista.add(new Kolo(2.0));
		lista.add(new Kolo(4.0));
		lista.add(new Kolo(6.0));
		
		
		for(Figura f1:lista) {
			System.out.println("Pole: "+f1.obliczPole());
		}
		
		
		
	}

}
