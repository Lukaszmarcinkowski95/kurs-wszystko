
public class Przyklad2 {

	public static void main(String[] args) {
		int[] tab = { 101 };
		int liczba = 15;
		Box box = new Box();
		box.liczba = 60;
		Box box2 = new Box();
		box2.liczba = 70;
		mutate(liczba, box, box2, tab);

		System.out.println("==============po zmianie=======");
		System.out.println("Tablica: " + tab[0]);//102
		System.out.println("liczba: " + liczba);//16
		System.out.println("Box: " + box.liczba);//61
		System.out.println("Box2: " + box2.liczba);//70

	}

	public static void mutate(int aa, Box bb, Box cc, int[] tt) {
		aa = aa + 1;
		bb.liczba += 1;
		cc = new Box();
		cc.liczba = 87;
		tt[0] += 1;

	}

}
