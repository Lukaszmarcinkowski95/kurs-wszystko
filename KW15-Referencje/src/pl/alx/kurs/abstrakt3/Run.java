package pl.alx.kurs.abstrakt3;

import java.util.ArrayList;
import java.util.List;

public class Run {

	public static void main(String[] args) {
		
		
		List<Figura> lista = new ArrayList<>();
		lista.add(new Kolo("Kolo roweru",2.0));
		lista.add(new Kolo("Podstawa garnka",4.0));
		lista.add(new Kolo("Plaska ziemia", 6.0));
		
//		Nie mozna powolac obiektu klasy abrstrakcyjnej
//		Figura figura = new Figura("cos");
		
		
		for(Figura f1:lista) {
			f1.przedstawSie();
			System.out.println("Pole: "+f1.obliczPole());
		}
		
		
		
	}

}
