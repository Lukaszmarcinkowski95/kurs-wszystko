package pl.alx.kurs.abstrakt3;

public abstract class Figura {
	
	
	String nazwa;

	public Figura(String nazwa) {

		this.nazwa=nazwa;
		
	}

	public void przedstawSie() {
		System.out.println("Nazywam sie: "+nazwa);
	}
	
	public abstract double obliczPole();
		
	
	
	
	
}
