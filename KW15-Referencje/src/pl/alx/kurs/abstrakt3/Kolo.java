package pl.alx.kurs.abstrakt3;

public class Kolo extends Figura {
	private double r;

	public Kolo(String nazwa, double r) {
		super(nazwa);
		this.r = r;
	}

	@Override
	public double obliczPole() {

		System.out.println("Promien: " + r);
		return r * r * Math.PI;
	}

}
