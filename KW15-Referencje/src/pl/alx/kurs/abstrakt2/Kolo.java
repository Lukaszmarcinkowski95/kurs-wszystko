package pl.alx.kurs.abstrakt2;

public class Kolo implements Figura {
	private double r;

	public Kolo(double r) {

		this.r = r;
	}

	@Override
	public double obliczPole() {

		System.out.println("Promien: " + r);
		return r * r * Math.PI;
	}

}
