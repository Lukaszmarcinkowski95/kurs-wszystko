package pl.alx.kurs.cwiczenie;

public abstract class Sportowiec {
	
	private double predkosc;
	private String imie;
		
	public double getPredkosc() {
		return predkosc;
	}


	public void setPredkosc(double predkosc) {
		this.predkosc = predkosc;
	}


	public String getImie() {
		return imie;
	}


	public void setImie(String imie) {
		this.imie = imie;
	}


	public abstract void przyspiesz();

}
