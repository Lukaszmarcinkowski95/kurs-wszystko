package pl.alx.kurs.cwiczenie;

public class Sprinter extends Sportowiec {

	
	
	double predkosc;
	String imie;
	public Sprinter() {
		super();	
	}
	
	public double getPredkosc() {
		return predkosc;
	}

	public void setPredkosc(double predkosc) {
		this.predkosc = predkosc;
	}

	@Override
	public void przyspiesz() {
		
		this.setPredkosc(this.getPredkosc()+5);
	}
	
	
	
	
	

}
