
public class Przyklad {

	public static void main(String[] args) {

		int a;
		a = 5;
		int b = 10;
		b = a;
		b = 25;

		System.out.println("a: " + a + " b: " + b);

		Box b1 = new Box();
		b1.liczba = 34;
		Box b2 = b1;
		b2.liczba = 45;

		System.out.println("b1: " + b1.liczba + " b2: " + b2.liczba);

		b2 = new Box();
		b2.liczba = 99;
		System.out.println("b1: " + b1.liczba + " b2: " + b2.liczba);

	}

}
