/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.alx.kalkulator;

/**
 *
 * @author kurs
 */
public class LogikaKalkulator {
    public static double obliczWynik(double liczba1, double liczba2, String operacja){
      double wynik = 0.0;

        switch (operacja) {
            case "+":
                wynik = liczba1 + liczba2;

                break;
            case "-":
                wynik = liczba1 - liczba2;

                break;
            case "*":
                wynik = liczba1 * liczba2;

                break;
            case "/":
                wynik = liczba1 / liczba2;

                break;
            case "^":
                wynik = Math.pow(liczba1, liczba2);
                break;
                
        }
        return wynik;
    
    
    
}
    
}
