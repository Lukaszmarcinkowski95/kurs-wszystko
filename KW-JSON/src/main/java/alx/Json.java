/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alx;

import com.google.gson.Gson;


/**
 *
 * @author kurs
 */
public class Json {
public static void main(String[] args){
    System.out.println("hi");
    
    Gson gson = new Gson();
    String json = gson.toJson(2);
    System.out.println("json: "+json);
    double i = 179.99;
    Osoba o1 = new Osoba("Tomek","Nowak",22, i);
    System.out.println("Osoba1: "+o1);
    String osobaWJson = gson.toJson(o1);
    System.out.println("Osoba w Json: "+osobaWJson);
    String znowuOsoba = "{\"imie\":\"Janek\",\"nazwisko\":\"Nowaczyk\",\"wiek\":26,\"wzrost\":279.99}";
    Osoba o2 = gson.fromJson(znowuOsoba, Osoba.class);
    System.out.println("Osoba z Jsona: "+o2);
    
}    


}
