/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.alx.kw26.nbp;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.alx.model.Tabela;

/**
 *
 * @author kurs
 */
public class ObslugaNBP {
    public static String URL = "http://api.nbp.pl/api/exchangerates/tables/";
    public static String format = "?format=json";
    
    
    public static Tabela[] pobierzTabele(String ktoraTabela, String data ){
        String pobraneDane="";
        try {
            pobraneDane = readFromWeb(URL+ktoraTabela+"/"+data+format);
            System.out.println(pobraneDane);
            return przetworzDane(pobraneDane);
        } catch (IOException ex) {
//            Logger.getLogger(ObslugaNBP.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        return przetworzDane(pobraneDane);
    }
    
    static Tabela[] przetworzDane(String dane){
        Gson gson = new Gson();
        return gson.fromJson(dane, Tabela[].class);        
    }
    
    
    
    
    
    public static String readFromWeb(String webURL) throws IOException {
        String data ="";
        URL url = new URL(webURL);
        InputStream is =  url.openStream();
        try( BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
                data+=line;
            }
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            throw new MalformedURLException("URL jest zjebany!!");
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new IOException();
        }
        return data;
    }
    
    
}
