/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.alx.okienka;


import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author kurs
 */
public class Proste5 {

    public static void main(String[] args) {
        JFrame okno = new JFrame("Pierwsze Okno");

        okno.setSize(600, 480);
        LayoutManager layout = new GridLayout();
       
        layout = new GridLayout(2,2);
         
        okno.setLayout(layout);
        
        
        Color kolor = Color.CYAN;
        kolor = new Color(120,120,120);  //Kolor -> wartosc RGB od 0 do 255
        kolor = new Color(100,200,50);
        kolor = new Color(0xF6CACA); //Wartoc szesnastkowa 0xWARTOSC_KOLORU
        okno.getContentPane().setBackground(kolor);
        
        
        JLabel etykieta = new JLabel("Pozdrowienia z Poznania");
        okno.add(etykieta);
        etykieta.setHorizontalAlignment(SwingConstants.CENTER);
        etykieta.setForeground(Color.YELLOW);
        Font czcionka = new Font("Arial", Font.BOLD, 30);
        etykieta.setFont(czcionka);
        
        JLabel etykieta2 = new JLabel("Ala ma kota");
        okno.add(etykieta2);
        etykieta2.setHorizontalAlignment(SwingConstants.CENTER);
        etykieta2.setForeground(Color.GREEN);        
        etykieta2.setFont(czcionka);
        
        
        
        JButton przycisk = new JButton("Nacisnij mnie");
        przycisk.setFont(new Font ("Arial", Font.ITALIC, 22));
        okno.add(przycisk);
        MojListener listener = new MojListener();
        przycisk.addActionListener(listener);
        
        ObslugaMyszki obslugaM = new ObslugaMyszki();
        okno.addMouseListener(obslugaM);
        
        
        
        
        
        
        
        

        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);

    }

    private static LayoutManager GridLayout(int i, int i0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
