package pl.alx.wyjatki.cwiczenie2;

import java.util.Scanner;

public class Run {

	public static void main(String[] args) {
		
		Osoba osoba;
		try {
			osoba = pobierzOsobe();
			System.out.println("Osoba: "+osoba);
			
		} catch (ZleDane e) {
			System.out.println("Przykro mi... zly wiek");
		}
		
		
		
	}

	public static Osoba pobierzOsobe() throws ZleDane {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Podaj imie: ");
		String imie = sc.nextLine();
		
		System.out.println("Podaj nazwisko: ");
		String nazwisko = sc.nextLine();
		
		System.out.println("Podaj wiek: ");
		double wiek = Double.parseDouble(sc.nextLine());

		Osoba osoba = new Osoba(imie, nazwisko, wiek);
		
		sc.close();
		
		if(wiek <= 0) {
			
			throw new ZleDane();
			
		}
		
		return osoba;
	}

}
