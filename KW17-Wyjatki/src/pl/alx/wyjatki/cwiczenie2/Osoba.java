package pl.alx.wyjatki.cwiczenie2;

public class Osoba {

	
	String imie, nazwisko;
	double wiek;
	
	public Osoba(String imie, String nazwisko, double wiek) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.wiek = wiek;
	}
	
	
	@Override
	public String toString() {
		return "Osoba [imie=" + imie + ", nazwisko=" + nazwisko + ", wiek=" + wiek + "]";
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public double getWiek() {
		return wiek;
	}
	public void setWiek(double wiek) {
		this.wiek = wiek;
	}
	
	
	
	
}
