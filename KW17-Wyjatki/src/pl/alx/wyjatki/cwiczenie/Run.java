package pl.alx.wyjatki.cwiczenie;

import java.util.Scanner;

public class Run {

	public static void main(String[] args) {

		int tab[] = { 1, 2, 3, 4, 5, 6 };

		int indeks;
		Scanner sc = new Scanner(System.in);

		try {
			System.out.println("Podaj ID indeksu tablicy 6-o elementowej: ");
			String indeksPobrany = sc.nextLine();
			indeks = Integer.parseInt(indeksPobrany) - 1;
			System.out.println("Element tablicy: " + tab[indeks]);
		} catch (NumberFormatException e) {

			System.out.println("Podany znak to nie cyfra!!");

		} catch (ArrayIndexOutOfBoundsException e) {
			
			System.out.println("To byla tablica 6-o Elementowa :)");
		}

	}

}
