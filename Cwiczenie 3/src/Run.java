import java.util.List;
import java.util.ArrayList;

public class Run {
	public static void main(String[] args) {
		
		List<Figura> lista = new ArrayList<>();
		
		lista.add(new Kolo("Kolo1", 3));
		lista.add(new Kolo("Kolo2", 4));
		lista.add(new Kolo("Kolo3", 5));
		lista.add(new Kolo("Kolo4", 6));
		lista.add(new Kolo("Kolo5", 7));
		lista.add(new Kolo("Kolo6", 8));
		lista.add(new Kolo("Kolo7", 9));
		lista.add(new Kolo("Kolo8", 10));
		lista.add(new Prostokat("Prostokąt1",3,4));
		lista.add(new Prostokat("Prostokąt2",4,5));
		lista.add(new Prostokat("Prostokąt3",5,6));
		lista.add(new Prostokat("Prostokąt4",6,7));
		lista.add(new Prostokat("Prostokąt5",7,8));
		lista.add(new Prostokat("Prostokąt6",8,9));
		lista.add(new Prostokat("Prostokąt7",9,10));
		
		
		for (Figura figura : lista){
			System.out.println("Jestem: "+figura.przedstawSie());
			System.out.println("Moje pole: "+figura.obliczPole());
			
			
		}
		
		
		
		
	}

}
