public class Prostokat extends Figura {
	
	private double bokA;
	private double bokB;
	
	public Prostokat (String nazwa, double bokA, double bokB) {
		super(nazwa);
		this.bokA=bokA;
		this.bokB=bokB;
	}
	
	
	
	@Override
	public double obliczPole() {
		
		double pole;
		System.out.println("Bok a: "+bokA+" bok b: "+bokB);
		pole = this.bokA*this.bokB;
		
		return pole;
	}
	

}
