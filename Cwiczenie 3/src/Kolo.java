
public class Kolo extends Figura {
	

	private double promien;

	public Kolo(String nazwa, double promien) {
		super(nazwa);
		this.promien=promien;
		

	}

	@Override
	public double obliczPole() {
		double pole;
		System.out.println("Promien: "+promien);
		pole = Math.PI * (this.promien * this.promien);

		return pole;
	}

}
