package pl.alx.kurs.kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Mapy {

	public static void main(String[] args) {
		
		
		Map<String, String> mapa = new HashMap<>();
		mapa.put("Ola", "Jola");
		mapa.put("Robert", "Ania");
		System.out.println(mapa);
		
		System.out.println("Pobranie: "+mapa.get("Robert"));
		
		System.out.println("Usun: "+mapa.remove("Robert"));
		System.out.println(mapa);
		
		
		
		Map<String,String> mapa2 = new HashMap<>();
		
		mapa2.put("Robert","Tola");
		mapa2.put("Robert", "Viola");
		
		mapa.putAll(mapa2);
		
		System.out.println(mapa);
		System.out.println();
		
		System.out.println("mapa.size: "+mapa.size());
		System.out.println("isEmpty: "+mapa.isEmpty());
		System.out.println("Czy zawiera klucz jan: "+mapa.containsKey("Jan"));
		System.out.println("Czy zawiera wartosc Jola:"+mapa.containsValue("Jola"));
		System.out.println("Wszystkie klucze: "+mapa.keySet());
		System.out.println("Wsystkie wartosci"+mapa.values());
		System.out.println();
		
		System.out.println("Iteracja kluczy for each");
		Set<String> zbiorKluczy = mapa.keySet();
		for(String k: zbiorKluczy) System.out.println(k);
		
		
		
	}

}
