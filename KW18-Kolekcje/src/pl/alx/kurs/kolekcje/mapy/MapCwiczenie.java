package pl.alx.kurs.kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapCwiczenie {

	public static void main(String[] args) {

		Map<String, Integer> mapa = new HashMap<>();
		mapa.put("Zero", 0);
		mapa.put("Jeden", 1);
		mapa.put("Dwa", 2);
		mapa.put("Trzy", 3);
		mapa.put("Cztery", 4);
		mapa.put("Piec", 5);
		mapa.put("Szesc", 6);

		String jeden = "Jeden";
		String piec = "Piec";

		Integer a, b;
		a = mapa.get(jeden);
		b = mapa.get(piec);
		System.out.println("Suma jeden i piec: " + (a + b));

		System.out.println("Kolekcja wartosci: " + mapa.values());

		System.out.println("Rozmiar: " + mapa.size());

		Set<String> zbiorKluczy = mapa.keySet();

		for (String s : zbiorKluczy) {
			System.out.println(s + " = " + mapa.get(s));
		}
	}

}
