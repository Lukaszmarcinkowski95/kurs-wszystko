package pl.alx.kurs.kolekcje;

import java.util.ArrayList;

public class Lista {

	public static void main(String[] args) {
		
		ArrayList<String> listaStr = new ArrayList<>();
		listaStr.add("Pies");
		listaStr.add("Kot");
		listaStr.add(0, "chomik");
		int rozmiar = listaStr.size();
		listaStr.remove("Pies");
		listaStr.remove(1);
		String coZostalo = listaStr.get(0);
		System.out.println(coZostalo);
		if (listaStr.contains("chomik")) System.out.println("Mam chomika na liscie");
		for (int i=0;i<listaStr.size();i++) {
			System.out.println("Element "+listaStr.get(i));
		}
		
		for(String element : listaStr) {
			System.out.println("Element: "+element);
		}
		
	}

}
