package pl.alx.kurs.kolekcje.cwiczenia;

import java.util.ArrayList;

public class ListaCwiczenie {

	public static void main(String[] args) {
		
		ArrayList<String> lista = new ArrayList<>();
		lista.add("A");
		lista.add("B");
		lista.add("C");
		lista.add("D");
		lista.add("E");
		
		System.out.println(lista);
		
		System.out.println("Element o indeksie 2:"+lista.get(2));
		
		lista.set(3, "A");
		
		System.out.println(lista);
		
		lista.remove("A");
		System.out.println(lista);
		 
		lista.remove(0);
		System.out.println(lista);
		
		if (lista.contains("D")) System.out.println("Lista zawiera D");
		else System.out.println("Lista nie zawiera D");
		
		System.out.println("Rozmiar listy to: "+lista.size()); 
		

	}

}
