package pl.alx.kurs.kolekcje.kolejki;

import java.util.ArrayDeque;

public class Kolejka {

	public static void main(String[] args) {

		ArrayDeque<String> kolejka = new ArrayDeque<>();
		kolejka.addLast("A");
		kolejka.addLast("B");
		kolejka.addFirst("C");
		System.out.println(kolejka);
		kolejka.add("D");
		System.out.println(kolejka);
		
		//===================================
		//FIFO i LIFO
		//FIFO ---> First in, First out
		//LIFO ---> Last in, First out
		//===================================
		
		System.out.println("Wyciagnij ostatni: "+kolejka.pollLast());
		System.out.println("Usun ostatni: "+kolejka.removeLast());
		System.out.println("Wielkosc: "+kolejka.size());
		System.out.println("Wez pierwszy: "+kolejka.getFirst());
		System.out.println(kolejka);
		
		kolejka.add("Z");
		kolejka.add("X");
		kolejka.add("Y");
		System.out.println(kolejka);
		
		String abc = kolejka.pop();
		System.out.println(kolejka);
		System.out.println(abc);
		
		
		
		
		
	}
}
