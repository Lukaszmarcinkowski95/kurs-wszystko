package pl.alx.kurs.kolekcje.kolejki;

import java.util.ArrayDeque;

public class KolejkaCwiczenie {
	public static void main(String[] args) {

		ArrayDeque<String> kolejka = new ArrayDeque<>();
		kolejka.add("A");
		kolejka.add("B");
		kolejka.add("C");
		kolejka.add("D");
		kolejka.add("E");

		System.out.println("Zadeklarowana kolejka");
		System.out.println(kolejka);
		System.out.println("=====================");

		kolejka.pop();
		kolejka.pop();

		System.out.println("Usuniete dwa elementy zgodnie z zasada FIFO");
		System.out.println(kolejka);
		System.out.println("=====================");

		kolejka.pollLast();
		kolejka.pollLast();

		System.out.println("Usuniete dwa elementy zgodnie z zasada LIFO");
		System.out.println(kolejka);

	}

}
