package pl.alx.kurs.kolekcje.zbiory;

import java.util.HashSet;
import java.util.Set;

public class Zbiory {

	public static void main(String[] args) {
		
		HashSet<String> zbior = new HashSet<>();
		zbior.add("Ola");
		zbior.add("Ala");
		zbior.add("Tola");
		zbior.add("Ula");
		
		
		System.out.println(zbior);
		
		
		for(String s: zbior) {
			System.out.println(s);
		}
		System.out.println("Wielkosc zbioru: "+zbior.size());
		System.out.println("Czy zbior jest pusty? :"+zbior.isEmpty());
		
		System.out.println("Czy zawiera Ala?  "+zbior.contains("Ala"));
		
		System.out.println("Usun Zosia: "+zbior.remove("Zosia"));
		
		System.out.println(zbior);
		
		
		Set<String> set = new HashSet<>();
		set.add("Tomek");
		set.add("Romek");
		
		
		
		
		zbior.addAll(set);
		
		System.out.println(zbior);
		
		
		

	}

}
