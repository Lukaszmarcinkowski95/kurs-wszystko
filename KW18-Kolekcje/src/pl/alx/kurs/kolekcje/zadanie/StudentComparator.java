package pl.alx.kurs.kolekcje.zadanie;

import java.util.Comparator;

public class StudentComparator implements Comparator<Student>{

	@Override
	public int compare(Student o1, Student o2) {
		int ilosc1 = o1.getIloscPiw();
		int ilosc2 = o2.getIloscPiw();
		if (ilosc1==ilosc2) return 0;
		else if (ilosc1>ilosc2) return 1;
		else return -1;
		
		
		
	}



}
