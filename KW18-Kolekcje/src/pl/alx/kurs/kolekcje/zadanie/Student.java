package pl.alx.kurs.kolekcje.zadanie;

public class Student {
	
	String imie;
	int iloscPiw;
	
	public Student(String imie, int iloscPiw) {
		super();
		this.imie = imie;
		this.iloscPiw = iloscPiw;
	}

	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public int getIloscPiw() {
		return iloscPiw;
	}
	public void setIloscPiw(int iloscPiw) {
		this.iloscPiw = iloscPiw;
	}

	@Override
	public String toString() {
		return "\nStudent [imie=" + imie + ", iloscPiw=" + iloscPiw + "]";
	}
	
	
	

}
