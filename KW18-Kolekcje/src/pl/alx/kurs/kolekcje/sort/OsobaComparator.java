package pl.alx.kurs.kolekcje.sort;

import java.util.Comparator;

public class OsobaComparator implements Comparator<Osoba> {

	@Override
	public int compare(Osoba o1, Osoba o2) {
//		double wiek1 = o1.getWiek();
//		double wiek2 = o2.getWiek();
//		if (wiek1 == wiek2) return 0; 
//		else if (wiek1<wiek2) return -1;
//		else return 1;
		return o1.getImie().compareTo(o2.getImie());
				}

}
