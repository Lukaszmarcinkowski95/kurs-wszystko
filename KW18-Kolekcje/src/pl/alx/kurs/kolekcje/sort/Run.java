package pl.alx.kurs.kolekcje.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Run {
	public static void main(String[] args) {
		List<Osoba> listaOsob = new ArrayList<>();
		
		Osoba o1 = new Osoba ("Jan","Kowalski",22);
		Osoba o2 = new Osoba ("Tomasz","Z Akwinu",32);
		Osoba o3 = new Osoba ("Jacek","Nowak",23);
		Osoba o4 = new Osoba ("Ala","MaKota",54);
		Osoba o5 = new Osoba ("Tomek","TenWysoki",17);
		listaOsob.add(o1);
		listaOsob.add(o2);
		listaOsob.add(o3);
		listaOsob.add(o4);
		listaOsob.add(o5);
		System.out.println(listaOsob);
		OsobaComparator mojComparator = new OsobaComparator();
		Collections.sort(listaOsob, mojComparator);
		System.out.println(listaOsob);
		
		
		
	}
}
