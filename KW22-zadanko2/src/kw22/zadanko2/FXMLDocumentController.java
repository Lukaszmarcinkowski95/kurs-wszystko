/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kw22.zadanko2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author kurs
 */
public class FXMLDocumentController implements Initializable { 
    @FXML
    private Label labelWynik;
    int numer =0;
  
    @FXML
    private void funkcjaDodaj(ActionEvent event) {
        
        labelWynik.setText((++numer)+"");
    }
    @FXML
    private void funkcjaPomnoz(ActionEvent event) {
        numer=numer*2;
        labelWynik.setText(numer+"");

    }
    @FXML
    private void funkcjaReset(ActionEvent event) {
        numer=0;
        labelWynik.setText(numer+"");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labelWynik.setText("0");
    }    
    
}
