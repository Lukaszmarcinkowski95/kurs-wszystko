package pl.alx.kurs.kpij.klasy4.przyklad;

public class Osoba {
	
	private String imie;
	private String nazwisko;
	
	
	
	
	
	public Osoba(String imie, String nazwisko) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
	}
	
	
	public void wyszlaczkowaneImie() {
		System.out.println("///////~~~~~~~~~~~~ "+imie);
	}
	
	
	public static void przykladowaFunkcjaStatyczna() {
		
		System.out.println("W funkcji statycznej");
		//funkcja statyczna nie dostepu do argumentow obiektu
		//Nie powiazana z obiektem
		//jej definicja znajduje sie w klasie. To jest jedyne powiazanie
		
	}
	
	
}
