package pl.alx.kurs.kpij.klasy4.polimorfizm;

import java.util.List;
import java.util.ArrayList;

public class Student extends Osoba {

	// Nadklasa -> osoba
	// przy dziedziczeniu jesli klasa bazowa nie zawiera konstruktor domyslnego ->()
	// musimy wywolac konstruktor nadklasy
	private int rokStudiow;
	private String kierunek;
	private List<Double> oceny = new ArrayList<>();

	public Student(int rokStududiow, String kierunek, String imie, String nazwisko, double wiek) {
		super(imie, nazwisko, wiek);
		this.setRokStudiow(rokStududiow);
		this.setKierunek(kierunek);

	}

	public String getKierunek() {
		return kierunek;
	}

	public void dodajOcene(Double ocena) {
		oceny.add(ocena);

	}

	public double wyliczSrednia() {

		double suma = 0.0;
		// for (int i=0;i<oceny.size();i++) {
		// double ocenaZListy = oceny.get(i);
		// suma+=ocenaZListy;
		// }
		for (Double ocena : oceny)
			suma += ocena;

		return suma / oceny.size();
	}
	
	public List<Double> pobierzListeOcen(){
		return oceny;
		
	}

	public void setKierunek(String kierunek) {
		this.kierunek = kierunek;
	}

	public int getRokStudiow() {
		return rokStudiow;
	}

	public void setRokStudiow(int rokStudiow) {
		this.rokStudiow = rokStudiow;
	}

	@Override // Informacja o nadpisaniu funkcji
	public void przedstawSie() {
		System.out.println("================================");
		super.przedstawSie();
		String napis = String.format("Jestem studentem. Nazywam się %s, Studiuje %s", getImie(), getKierunek());
		System.out.println(napis);
		System.out.println("================================");

	}

	@Override
	public String coRobisz() {
		return "Studiuję :X";
	}

	@Override
	public String toString() {
		return "Student [rokStudiow=" + rokStudiow + ", kierunek=" + kierunek + ", toString()=" + super.toString()
				+ "]";
	}
	

}
