package pl.alx.kurs.kpij.klasy4.polimorfizm;


public class Polimorfizm1 {

	public static void main(String[] args) {
		Osoba osoba1 = new Osoba("Tomasz","Kowalski",31.0);
		Student st = new Student(1, "Ekonomia", "Piotr", "Kowalski", 24);
		st.dodajOcene(2.0);
		st.dodajOcene(3.0);
		
		System.out.println(osoba1.getImie());
		System.out.println(st.getImie());
		System.out.println(st.wyliczSrednia());
		
		
		//===========================================
		
		//Typ osoba jest bardziej ogolny wiec kazdy student jest osoba.
		Osoba nowaOsoba = new Student(2,"Socjologia","Jas","Fasola", 23);
		System.out.println(nowaOsoba.getImie());
		nowaOsoba.przedstawSie();
		System.out.println(nowaOsoba.coRobisz());
		
		// Student jakisStudent = new Osoba("Tomek","Wolny",23);       <======== tak sie nie da. Typ student jest bardziej szzcegolowy od typu osoba.
		
		
		
		
		
		
		
	}

}
