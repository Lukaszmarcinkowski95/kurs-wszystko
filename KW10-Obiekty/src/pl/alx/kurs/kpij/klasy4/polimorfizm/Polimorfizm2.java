package pl.alx.kurs.kpij.klasy4.polimorfizm;

public class Polimorfizm2 {

	public static void zbadajOsobe(Osoba osoba) {

		// System.out.println(osoba.getImie());
		System.out.println("======================================");
		if (osoba instanceof Student) {
			System.out.println(osoba.getImie() + " Jest studentem");
			Student jakisStudent = (Student) osoba;
			System.out.println(jakisStudent.getKierunek());
			System.out.println(jakisStudent.wyliczSrednia());

		} else {
			System.out.println(osoba.getImie() + " Nie jest studendem");
			//Student abs = (Student) osoba;  <====== blad
			
		}

	}

	public static void main(String[] args) {

		Osoba[] osobyNaPochodzie = { new Osoba("Ala", "Kowalska", 25.0),
				new Student(1, "Chemia", "Sebek", "Nowak", 21.0), new Osoba("Halyna", "Kowalska", 35.0),
				new Student(1, "Chemia", "Pjoter", "Nowak", 27.0), new Osoba("Bożydar", "Kowalski", 55.0),
				new Student(1, "Chemia", "Ferdek", "Kiepski", 23.0)

		};

		for (Osoba osoba : osobyNaPochodzie) {
			// System.out.println(osoba);
			zbadajOsobe(osoba);
			Osoba.przedstawSie();
		}

	}

}
