package pl.alx.kurs.kpij.klasy4.dziedziczenie;

public class Przyklad {

	public static void main(String[] args) {

		Student st = new Student(1, "Hotelarstwo", "Jan", "Nowak", 22.5);
		String imieStudenta = st.getImie();
		String nazwiskoStudenta = st.getNazwisko();
		System.out.println("Imie: " + imieStudenta);
		System.out.println("Nazwisko: " + nazwiskoStudenta);
		System.out.println("Kierunek: " + st.getKierunek());
		System.out.println("Rok Studiów: " + st.getRokStudiow());
		System.out.println();

		st.przedstawSie();
		System.out.println(st.coRobisz());

		System.out.println("Koniec programu");

	}

}
