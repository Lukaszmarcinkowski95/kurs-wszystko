package pl.alx.kurs.kpij.klasy4.dziedziczenie;

public class WyliczSrednia {

	public static void main(String[] args) {

		Student st = new Student(1, "Ekonomia", "Piotr", "Kowalski", 24);
		st.dodajOcene(2.0);
		st.dodajOcene(3.0);
		st.dodajOcene(4.0);
		st.dodajOcene(5.0);
		st.dodajOcene(4.0);
		st.dodajOcene(2.0);

		System.out.println(st.wyliczSrednia());
		System.out.println(st.pobierzListeOcen());

	}

}
