package pl.alx.kurs.kpij.klasy1.atrybuty;

public class Zakupy {

	public static void main(String[] args) {
		
		
		System.out.println("Tworzymy obiekty klasy Osoba... ");
		System.out.println("");
		
		Osoba osobaPierwsza = new Osoba();
		Osoba osobaDruga = new Osoba();
		
		osobaPierwsza.imie = "Ala";
		osobaPierwsza.nazwisko = "Nowak";
		osobaPierwsza.wiek = 16;
		
		osobaDruga.imie = "Tomek";
		osobaDruga.nazwisko = "Nowak";
		osobaDruga.wiek = 34;
		
		
		Sklep biedronka = new Sklep();
		biedronka.nazwaSklepu = "Czerwona Stonka";
		
		
		biedronka.obsluz(osobaPierwsza);
		System.out.println("");
		biedronka.obsluz(osobaDruga);
		
				
		
				
		
		
		
		
		
		
	}

}
