package pl.alx.kurs.kpij.klasy1.atrybuty;

public class Program {

	public static void main(String[] args) {
		
		System.out.println("hello world");
		Osoba osoba1;
		osoba1 = new Osoba();
		osoba1.imie = "Janek";
		osoba1.nazwisko = "Nowak";
		osoba1.wiek = 76;
		
		Osoba osoba2 = new Osoba();
		osoba2.imie = "Tomek";
		osoba2.nazwisko = "Kowalski";
		osoba2.wiek = 21.2;
				
		System.out.println("Wypisuje osobe 2 po przypisaniu: ");
		System.out.println("Imie : "+osoba2.imie);
		System.out.println("Nazwisko : "+osoba2.nazwisko);
		System.out.println("Wiek : "+osoba2.wiek);
		
		
		System.out.println("Osoba2: "+osoba2);
		
		Osoba osoba3 = new Osoba();
		
		System.out.println("Wypisuje osobe3 po utworzeniu: ");
		System.out.println("Imie : "+osoba3.imie);
		System.out.println("Nazwisko : "+osoba3.nazwisko);
		System.out.println("Wiek : "+osoba3.wiek);
		
		
		////////////
		
		Osoba osoba4 = null;
		if (osoba4 != null)
		osoba4.imie = "Jakies imie"; 
//		Jesli obiekt jest przypisany jako null to obiekt jest pusty i nie mozna na nim dzialac. Wyskoczy null pointer expection
		
		
		System.out.println("Koniec");
		
		
		
		
		
		
		
		
	}

}
