package pl.alx.kurs.kpij.klasy.cwiczenia1;

import javax.swing.JOptionPane;

public class Cwiczenie2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String markaS, modelS, rocznikPobrane;
		markaS = JOptionPane.showInputDialog("Podaj markę pierwszego samochodu");	
		modelS = JOptionPane.showInputDialog("Podaj model pierwszego samochodu");	
		rocznikPobrane = JOptionPane.showInputDialog("Podaj rocznik pierwszego samochodu");	
		int rocznikS = Integer.parseInt(rocznikPobrane);
		Samochod auto1 = new Samochod(markaS,modelS,rocznikS);
		
		markaS = JOptionPane.showInputDialog("Podaj markę drugiego samochodu");	
		modelS = JOptionPane.showInputDialog("Podaj model drugiego samochodu");	
		rocznikPobrane = JOptionPane.showInputDialog("Podaj rocznik drugiego samochodu");	
		rocznikS = Integer.parseInt(rocznikPobrane);
		Samochod auto2 = new Samochod(markaS,modelS,rocznikS);
		
		
		System.out.println("Samochod 1: ");
		System.out.println(auto1);
		System.out.println();
		
		System.out.println("Samochod 2: ");
		System.out.println(auto2);
		System.out.println();
		
		
		
			
		

	}

}
