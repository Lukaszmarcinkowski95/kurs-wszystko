package pl.alx.kurs.kpij.klasy2.metody;

public class Program3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Osoba o1 = new Osoba("Ala", "Nowak", 22.0);
		Osoba o2 = new Osoba("Tomek", "Nowak", 34.5);

		String s1 = o1.toString();
		String s2 = o2.toString();
		System.out.println("Wynik dzialania metody toString: ");
		System.out.println(s1);
		System.out.println(s2);

		System.out.println("Proba wypisaia obiektu");
		System.out.println(o1);
		System.out.println(o2);
		System.out.println("");

		System.out.println("Kontaktacja");
		System.out.println("Osoba1: " + o1);
		System.out.println("Osoba2: " + o2);
		System.out.println("");

		System.out.println("print f");
		System.out.printf("Osoba1: %s\n", o1);
		System.out.printf("Osoba2: %s\n", o2);
		System.out.println("");

		System.out.println("ValueOf");
		System.out.println(String.valueOf(o1));
		System.out.println(String.valueOf(o2));
		System.out.println("");

	}

}
