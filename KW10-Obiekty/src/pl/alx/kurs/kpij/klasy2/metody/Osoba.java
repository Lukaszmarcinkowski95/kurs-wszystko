package pl.alx.kurs.kpij.klasy2.metody;

public class Osoba {

	String imie;
	String nazwisko;
	double wiek;

	// Konstruktor domyslny istnieje jesli zaden inny nie jest zdefiniowany
	// Jezeli mamy zdefiniowany dowolny kostruktor z parametrami, konstruktor
	// domyslny juz nie jest generowany
	// Nie mozna tworzyc kostruktorow o takiej samej kolejnosci typow zmiennych np.
	// w obu konstruktorach String String double. Ale mozna zmienic
	// na double String String...

	// Definicja kontruktora. Konstruktor musi sie nazywac tak samo jak klasa
	Osoba() {
		System.out.println("Jestem w kostruktorze klasy osoba");

	}

	Osoba(String i) {

		imie = i;
	}

	Osoba(double z, String b, String v) {
		wiek = z;
		imie = b;
		nazwisko = v;

	}

	Osoba(String i, String n, double w) {

		// Trzeba podpisac pod zmienne glowne
		imie = i;
		nazwisko = n;
		wiek = w;

	}

	void przedstawSie() {

		System.out.println("Moje imie:: " + imie);
		System.out.println("Moje nazwisko: " + nazwisko);
		System.out.println("Mam " + wiek + " lat");
		System.out.println("");

	}

	public String toString() {

		// String reprezentacjaWString = "Imie: " + imie + " Nazwisko: " + nazwisko + "
		// Wiek:" + wiek;
		// return reprezentacjaWString;
		String str = String.format("Imie %s, Nazwisko %s, Wiek: %.1f", imie, nazwisko, wiek);
		return str;

	}

}
