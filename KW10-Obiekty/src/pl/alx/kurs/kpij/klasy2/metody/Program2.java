package pl.alx.kurs.kpij.klasy2.metody;

public class Program2 {

	public static void main(String[] args) {

		Osoba o1 = new Osoba("Ala", "Nowak", 22.0);
		o1.przedstawSie();

		Osoba o2 = new Osoba();
		o2.imie = "Tomek";
		o2.nazwisko = "Nowak";
		o2.wiek = 33.5;
		o2.przedstawSie();

		Osoba o3 = new Osoba();
		o3.imie = "Ala";
		o3.przedstawSie();

		Osoba o4 = new Osoba(24, "Ola", "Kowalska");
		o4.przedstawSie();

	}

}
