package pl.alx.kurs.kpij.klasy2.metody;

public class Program {

	public static void main(String[] args) {

		Osoba o1 = new Osoba();
		o1.imie = "Ala";
		o1.nazwisko = "Nowak";
		o1.wiek = 22.0;
		
		
		
		o1.przedstawSie();
		
		
		Osoba o2 = new Osoba();
		o2.imie = "Tomek";
		o2.nazwisko = "Nowak";
		o2.wiek = 34.5;
		

		System.out.println("");
		
		
		o2.przedstawSie();
		

	}

}
