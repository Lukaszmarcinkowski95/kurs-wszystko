package pl.alx.kurs.kpij.klasy3.modyfikatory;


public class Program {

	public static void main(String[] args) {
		
		Osoba o1 = new Osoba("Ala", "Nowak", 22.0);
		Osoba o2 = new Osoba("Tomek", "Nowak", 34.5);

		System.out.println(o1);
		System.out.println(o2);
		o2.setImie("Zenek");
		
		//uzycie gettera
		String imieZO2 = o2.getImie();
		System.out.println("Imie z o2: " + imieZO2);
		
		
		Sklep zabka = new Sklep("Zabka");
		zabka.obsluz(o2);
		
		
		

	}

}
