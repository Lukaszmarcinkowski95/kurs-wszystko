package pl.alx.kurs.kpij.klasy3.modyfikatory;

public class Osoba {

	private String imie;
	private String nazwisko;
	private double wiek;

	Osoba(String imie, String nazwisko, double wiek) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.wiek = wiek;

	}

	@Override
	public String toString() {
		return "Osoba [imie=" + imie + ", nazwisko=" + nazwisko + ", wiek=" + wiek + "]";
	}

	public double getWiek() {
		return wiek;

	}

	public String getNazwisko() {
		return nazwisko;

	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;

	}

}
