package pl.alx.kurs.obiekty;

public class Osoba {

	protected String imie;

	protected String nazwisko;
	protected double wiek;

	public Osoba(double wiek, String imie, String nazwisko) {

		this.wiek = wiek;
		this.imie = imie;
		this.nazwisko = nazwisko;

	}
	
	
//	
//	public void przedstawSie() {
//		String napis = String.format("Nazywam sie %s %s i mam %0.f lat",getImie(),getNazwisko(),getWiek());
//		System.out.println(napis);
//		
//		
//		
//	}
	
	

	@Override
	public String toString() {
		return "Osoba [imie=" + imie + ", nazwisko=" + nazwisko + ", wiek=" + wiek + "]";
	}







	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public double getWiek() {
		return wiek;
	}

	public void setWiek(double wiek) {
		this.wiek = wiek;
	}

}
