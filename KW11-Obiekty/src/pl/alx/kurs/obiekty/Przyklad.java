package pl.alx.kurs.obiekty;

import java.util.ArrayList;
import java.util.List;

public class Przyklad {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Osoba o1 = new Osoba(50, "Jan", "Nowak");
		System.out.println(o1);

		Pracownik p1 = new Pracownik(42000, 12, "Fasola", "Jas", 42);
		System.out.println(p1);

		Szef s1 = new Szef(120000, 42, "Kiepski", "Ferdek", 78, 15000);
		System.out.println(s1);

		Szef s2 = new Szef(120000, 42, "Tak", "Halynka", 78);
		System.out.println(s2);

		System.out.println("Pensja s1: " + (s1.getPensja() + s1.getPremia()));
		System.out.println("Pensja s2: " + (s2.getPensja() + s2.getPremia()));

		List<Osoba> mojaListaZnajomych = new ArrayList<>();

		mojaListaZnajomych.add(o1);
		mojaListaZnajomych.add(p1);
		mojaListaZnajomych.add(s1);
		mojaListaZnajomych.add(s2);

		
		
		for (Osoba os : mojaListaZnajomych) {
			
			System.out.println("Znam: "+os.getImie());
			
			
			
		}
	}

}
