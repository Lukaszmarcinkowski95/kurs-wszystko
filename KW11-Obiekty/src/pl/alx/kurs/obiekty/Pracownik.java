package pl.alx.kurs.obiekty;

public class Pracownik extends Osoba {

	protected double pensja;
	protected int stazPracy;

	public Pracownik(double pensja, int stazPracy, String imie, String nazwisko, double wiek) {
		super(wiek, nazwisko, imie);
		this.pensja = pensja;
		this.stazPracy = stazPracy;

	}

	@Override
	public String toString() {
		return "Pracownik [pensja=" + pensja + ", stazPracy=" + stazPracy + ", toString()=" + super.toString() + "]";
	}

	public double getPensja() {
		return pensja;
	}

	public void setPensja(double nowaPensja) {
		this.pensja = nowaPensja;
	}

	public int getStazPracy() {
		return stazPracy;
	}

	public void setStazPracy(int stazPracy) {
		this.stazPracy = stazPracy;
	}

}
