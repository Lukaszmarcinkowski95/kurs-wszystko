package pl.alx.kurs.obiekty;

public class Szef extends Pracownik {

	protected double premia;

	public Szef(double pensja, int stazPracy, String imie, String nazwisko, double wiek) {
		super(pensja, stazPracy, imie, nazwisko, wiek);

	}
	public Szef(double pensja, int stazPracy, String imie, String nazwisko, double wiek, double premia) {
		super(pensja, stazPracy, imie, nazwisko, wiek);
		this.premia = premia;

	}


	@Override
	public String toString() {
		return "Szef [premia=" + premia + ", toString()=" + super.toString() + "]";
	}

	public double getPremia() {
		return premia;
	}

	public void setPremia(double premia) {
		this.premia = premia;
	}

}
