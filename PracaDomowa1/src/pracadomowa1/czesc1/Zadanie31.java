package pracadomowa1.czesc1;

public class Zadanie31 {
    public static void f(int a) {
        System.out.println(a * 2);
    }
    
    public static void main(String[] args) {
        f(3);
        f(5);
        f(7);
    }
    
}
