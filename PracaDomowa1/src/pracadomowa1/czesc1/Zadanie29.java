package pracadomowa1.czesc1;

public class Zadanie29 {
    public static void f() {
        int x = 3;
        x++;
        System.out.println(x);
    }
    
    public static void main(String[] args) {
        int x = 5;
        f();
        System.out.println(x);
        x++;
        f();
        System.out.println(x);
        x++;
    }
    
}
