package pracadomowa1.czesc1;

public class Zadanie48 {
    public static int f(int x) {
        System.out.println(x + 1);
        return x - 1;
    }
    
    public static void main(String[] args) {
        int x = f(3) + f(5) + f(7);
        System.out.println(x);
    }
    
}
