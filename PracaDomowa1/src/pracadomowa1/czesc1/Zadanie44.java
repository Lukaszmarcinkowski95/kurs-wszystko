package pracadomowa1.czesc1;

public class Zadanie44 {
    public static int f() {
        int x = 3;
        System.out.println(x + 1);
        return x - 1;
    }
    
    public static void main(String[] args) {
        int x = f() + f();
        System.out.println(x);
    }
    
}
