package pracadomowa1.czesc1;

public class Zadanie27 {
    public static void f() {
        int x = 3;
        System.out.println(x);
        x++;
    }
    
    public static void main(String[] args) {
        int a = 5;
        f();
        System.out.println(a);
        a++;
        f();
        System.out.println(a);
        a++;
    }
    
}
