package pracadomowa1.czesc1;

public class Zadanie34 {
    public static void f(int a, int b) {
        System.out.println(b - a);
    }
    
    public static void main(String[] args) {
        int x = 3;
        f(x, x + 2);
    }
    
}
