package pracadomowa1.czesc1;

public class Zadanie28 {
    public static void f() {
        int x = 3;
        x++;
        System.out.println(x);
    }
    
    public static void main(String[] args) {
        int a = 5;
        f();
        System.out.println(a);
        a++;
        f();
        System.out.println(a);
        a++;
    }
    
}
