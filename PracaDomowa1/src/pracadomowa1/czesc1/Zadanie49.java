package pracadomowa1.czesc1;

public class Zadanie49 {
    public static int x;
    
    public static int f() {
        x++;
        System.out.println(x + 1);
        return x - 1;
    }
    
    public static void main(String[] args) {
        int a = f();
        System.out.println(a);
    }
    
}
