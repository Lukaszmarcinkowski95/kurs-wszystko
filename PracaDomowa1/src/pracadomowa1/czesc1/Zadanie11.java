package pracadomowa1.czesc1;

public class Zadanie11 {

    public static void main(String[] args) {
        int a = 3;
        int b = 5;
        if (a > 1 && b < 1) {
            a += b;
        } else if (a < 1 || b > 1) {
            b += a;
        } else {
            a = b;
        }
        System.out.println(a);
        System.out.println(b);
    }
    
}
