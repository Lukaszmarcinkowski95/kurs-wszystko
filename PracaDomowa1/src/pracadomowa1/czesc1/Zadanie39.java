package pracadomowa1.czesc1;

public class Zadanie39 {
    public static void f(int a, int b) {
        a++;
        b--;
        System.out.println(b - a);
    }
    
    public static void main(String[] args) {
        int a = 3;
        int b = 5;
        f(b, a);
        System.out.println(a);
        System.out.println(b);
    }
    
}
