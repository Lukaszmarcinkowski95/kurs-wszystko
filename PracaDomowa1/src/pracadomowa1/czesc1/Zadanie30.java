package pracadomowa1.czesc1;

public class Zadanie30 {
    public static void h() {
        int x = 357;
        System.out.println(x);
    }
    
    public static void g() {
        int x = 7;
        h();
        h();
        System.out.println(x);
    }
    
    public static void f() {
        int x = 5;
        g();
        g();
        System.out.println(x);
    }
    
    public static void main(String[] args) {
        int x = 3;
        f();
        f();
        System.out.println(x);
    }
    
}
