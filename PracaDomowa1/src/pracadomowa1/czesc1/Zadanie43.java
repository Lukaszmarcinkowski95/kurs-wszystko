package pracadomowa1.czesc1;

public class Zadanie43 {
    public static int f() {
        int x = 3;
        int y = 5;
        System.out.println(y - x);
        return y + x;
    }
    
    public static void main(String[] args) {
        int x = f();
        System.out.println(x);
    }
    
}
