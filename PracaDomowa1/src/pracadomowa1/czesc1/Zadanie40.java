package pracadomowa1.czesc1;

public class Zadanie40 {
    public static void f(int a, int b, int c) {
        a++;
        b--;
        c += a;
        System.out.println(b - a + c);
    }
    
    public static void main(String[] args) {
        int a = 3;
        int b = 5;
        int c = 7;
        f(c, a, b);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
    
}
