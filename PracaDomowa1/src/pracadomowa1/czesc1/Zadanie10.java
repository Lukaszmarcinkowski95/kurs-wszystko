package pracadomowa1.czesc1;

public class Zadanie10 {

    public static void main(String[] args) {
        int a = 3;
        int b = 5;
        if (a > b) {
            a += b;
        } else {
            b += a;
        }
        if (b > 1) {
            b = 1000;
        }
        if (b > 100) {
            b = 10000;
        }
        if (b > 1000) {
            b = 100000;
        }
        System.out.println(a);
        System.out.println(b);
    }
    
}
