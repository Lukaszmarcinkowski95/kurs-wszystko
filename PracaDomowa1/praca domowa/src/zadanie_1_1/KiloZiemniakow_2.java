package zadanie_1_1;

import javax.swing.JOptionPane;

public class KiloZiemniakow_2 {
	public static void main(String[] args) {
		String cenaPobrane = JOptionPane.showInputDialog("Podaj ile kosztuje kilo ziemniakow");
		String iloscPobrane = JOptionPane.showInputDialog("Ile kg ziemniakow chcesz kupic?");
		double cena = Double.parseDouble(cenaPobrane);
		int ilosc = Integer.parseInt(iloscPobrane);
		JOptionPane.showMessageDialog(null, "Za ziemniaki zaplacisz: " + String.format("%.2f", (cena*ilosc)) + "zl");
		
	}
}
