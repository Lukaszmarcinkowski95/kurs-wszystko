package pl.alx.petle;

public class ForEach {

	public static void main(String[] args) {

		String[] tablicaStr = { "Ala", "Ola", "Ela", "Ula" };
		System.out.println("Wielkosc tablicy: " + tablicaStr.length);
		for (int i = 0; i < 4; i++)
			System.out.println("Element tablicy " + i + ": " + tablicaStr[i]);
		// Zwykla petla for ( ; ; )
		// Petla for each for ( : )

		for (String element : tablicaStr)
			System.out.println("Element : " + element);

		int[] tablicaInt = { 3, 33, 333, 33333, 44, 444, 4444, 44444 };
		for (int elementInti : tablicaInt)
			System.out.println("Element: " + elementInti);

	}

}
