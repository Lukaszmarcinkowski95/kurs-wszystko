package pl.alx.petle;

import java.util.Arrays;

public class Tablica {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		int tab[] = {2,3,4};
		for (int e : tab) {
			System.out.println(e);
		}
		String tabWstr = Arrays.toString(tab);   //zmiana liczb w tablicy na tekst
		System.out.println(tabWstr); //wywolanie elementow tablicy jako tekst
		System.out.println(tab); 	//wywolanie elementow tablicy bez zamiany na tekst
		
		
		int tablica[] = new int[10];
		System.out.println(Arrays.toString(tablica));
		tablica[4] = 654;
		System.out.println(Arrays.toString(tablica));
		
		String tabStr[] = new String[10];
		tabStr[1]="cos";
		System.out.println(Arrays.toString(tabStr));
		
		boolean tabBo[] = new boolean[10];
		System.out.println(Arrays.toString(tabBo));
		
		
	}

}
