package pl.alx.petle;

public class TabliceFunkcje {

	public static int suma(int tablica[]) {

		int wynik = 0;
		for (int i = 0; i < tablica.length; i++)
			wynik = wynik + tablica[i];
		return wynik;
	}

	// public static double srednia(int tablica[]) {
	//
	// double wynik = 0;
	//
	// for (int i = 0; i < tablica.length; i++)
	// wynik = wynik + tablica[i];
	// wynik = wynik / tablica.length;
	//
	// return wynik;
	// }

	public static double srednia(int tablica[]) {
		return (double) suma(tablica) / tablica.length;

	}

	public static int max(int tablica[]) {
		int wynik = tablica[0];

		for (int i = 0; i < tablica.length; i++) {
			if (tablica[i] >= wynik) {
				wynik = tablica[i];
			}
		}

		return wynik;
	}

	public static int sortowanie_b(int tablica[]) {

		int a;
		int zmiana = 1;

		while (zmiana > 0) {
			zmiana =0;
			for (int i = 0; i < tablica.length-1; i++) {
				if (tablica[i] > tablica[i + 1]) {
					a = tablica[i + 1];
					tablica[i + 1] = tablica[i];
					tablica[i] = a;
					zmiana++;

				}

			}

		}
		for (int i = 0; i < tablica.length; i++)
			System.out.println(tablica[i]);
		return 0;
	}

	public static void main(String[] args) {
		int A[] = { 33, 333, 3333, 22, 222 };
		int B[] = { 11, 44, 55, 101, 99, 100 };
		int C[] = { -4, -6, 12, 7 };
		int D[] = {};

		System.out.println("Suma tablicy A " + suma(A));
		System.out.println("Suma tablicy B " + suma(B));
		System.out.println("Suma tablicy C " + suma(C));
		System.out.println("Srednia tablicy A " + srednia(A));
		System.out.println("Srednia tablicy B " + srednia(B));
		System.out.println("Srednia tablicy C " + srednia(C));
		System.out.println("Wartosc maxymalna tablicy A " + max(A));
		System.out.println("Wartosc maxymalna tablicy B " + max(B));
		System.out.println("Wartosc maxymalna tablicy C " + max(C));
//		System.out.println("Suma tablicy D " + suma(D));
//		System.out.println("Srednia tablicy D " + srednia(D));
//		System.out.println("Wartosc maxymalna tablicy D " + max(D));
		sortowanie_b(A);
		System.out.println("=============");
		sortowanie_b(B);
		System.out.println("===============");
		sortowanie_b(C);
		

	}

}
