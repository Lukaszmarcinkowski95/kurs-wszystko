package pl.alx.petle;

public class For {
	public static int dodajDwa(int x) {
		System.out.println("Dodaje dwa do liczby: " + x);
		return x + 2;
	}

	public static void main(String[] args) {

		// int xyz = 2;
		// xyz *= 4;
		// xyz = 3 + 3;
		// int zzz = dodajDwa(xyz);
		// System.out.println(zzz);

		/*
		 * for (int i = 0; i < 10; i++) { System.out.println("Petla for");
		 * System.out.println("Napis"); System.out.println("Obrot: " + (i + 1)); }
		 * System.out.println("po petli");
		 */

		// int ileObrotow = 0;
		// for (int i = 0; i < 5; i++) { //Uwaga na sredniki for i nawiasy albo for i
		// pierwszy srednik
		// System.out.println("Petla 2");
		// ileObrotow = i;
		// }
		// System.out.println("Obroty: "+ ileObrotow);
		// System.out.println("Po petli");

		// int j = 0;
		// for (;;) {
		// if (j == 10) break;
		// j++;
		// System.out.println(j);
		//
		// }
		// System.out.println("Po petli");
		//
		// for(WYWOLANIE_POCZATKOWE ; WARUNEK PETLI ; INSTRUKCJA DO WYKONANIA PO
		// OBROCIE)
		//
		// for (int jj = 0; jj < 5; jj++) System.out.println("Witaj po raz:"+(jj+1));

		//
		long liczba = 0;
		for (long i = 0; i < 1000; i++) {
			if (i % 2 != 0) {
				liczba = liczba + i;
			

			}
		}
		System.out.println("Kolejna suma: " + (liczba));
		
		
		liczba =0;
		
		
		for (long i = 1; i <= 1000; i=i+2) {
			
			liczba = liczba + i;
	
	}
		System.out.println("Kolejna suma: " + (liczba));


	}

}
