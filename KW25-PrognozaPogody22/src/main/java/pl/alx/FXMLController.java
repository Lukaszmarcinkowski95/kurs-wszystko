package pl.alx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import pl.alx.pogoda.model.Pogoda;

public class FXMLController implements Initializable {
    
    @FXML
    private Label labelPogoda;
    @FXML
    private TextField textMiasto;
    @FXML
    private Label labelCisnienie;
        @FXML
    private Label labelWiatr;
    
    
    @FXML
    private void pobierzPogode(ActionEvent event) {
        String miasto = textMiasto.getText();
        Pogoda pobranaPogoda = PobieraczPogody.pobierzPogode(miasto);
        labelPogoda.setText(pobranaPogoda.getMain().getTemp()+"");
        labelCisnienie.setText(pobranaPogoda.getMain().getPressure()+"");
        labelWiatr.setText(pobranaPogoda.getWind().getSpeed()+"");
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
}
