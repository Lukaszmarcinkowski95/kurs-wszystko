package pl.alx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLController implements Initializable {
    
    @FXML
    private Label labelPogoda;
    @FXML
    private TextField textMiasto;
    
    
    @FXML
    private void pobierzPogode(ActionEvent event) {
        String miasto = textMiasto.getText();
        double temp = PobieraczPogody.pobierzTemperature(miasto);
        labelPogoda.setText(temp+"");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
}
