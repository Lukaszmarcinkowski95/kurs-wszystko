package postgres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Odczyt1 {
	public static void main(String[] args) {

		// jdbc - Java DataBase Connection

		try {
			String connString = "jdbc:postgresql://192.168.8.110:5432/hr";
			Connection c = DriverManager.getConnection(connString, "postgres", "kurs");
			System.out.println("Nawiazalem polaczenie c= " + c);
			Statement stm = c.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM employees");
			System.out.println("Mam wyniki rs= " + rs);
			int i = 1;
			while (rs.next()) {

				// System.out.println("Wynik: "+rs.getString(2)+" "+rs.getString(3));
				System.out.println(i+". Imie: " + rs.getString("first_name") 
						+ " Nazwisko: " + rs.getString("last_name")
						+ " Wyplata: " + rs.getInt("salary") 
						+ " Dzial: " + rs.getInt("department_id")
						+ " Data zatrudnienia: " + rs.getString("hire_date")
						);
				i++;
			}
			
			c.close();
			
			
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

}
