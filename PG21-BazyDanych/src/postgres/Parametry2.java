package postgres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Parametry2 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Podaj minimalna pensje: ");
		int minimalna = sc.nextInt();

		System.out.println("Podajna maxymalna pensje: ");
		int maxymalna = sc.nextInt();
		String connString = "jdbc:postgresql://localhost:5432/hr";
		try (Connection c = DriverManager.getConnection(connString, "postgres", "kurs")) {

			String sql = "SELECT * FROM  employees" + " WHERE salary > ?"
					+ " AND salary < ?";
			
			try (PreparedStatement stm = c.prepareStatement(sql)) {
				stm.setInt(1, minimalna);
				stm.setInt(2, maxymalna);
				ResultSet rs = stm.executeQuery();

				int i = 1;
				while (rs.next()) {

					// System.out.println("Wynik: "+rs.getString(2)+" "+rs.getString(3));
					System.out.println(i + ". Imie: " + rs.getString("first_name") + " Nazwisko: "
							+ rs.getString("last_name") + " Wyplata: " + rs.getInt("salary") + " Dzial: "
							+ rs.getInt("department_id") + " Data zatrudnienia: " + rs.getString("hire_date"));
					i++;
				}
				System.out.println();

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sc.close();

	}

	private static String sql() {
		// TODO Auto-generated method stub
		return null;
	}
}
