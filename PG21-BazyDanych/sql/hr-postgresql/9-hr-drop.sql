DROP INDEX emp_name_ix;
DROP INDEX emp_manager_ix;
DROP INDEX emp_job_ix;
DROP INDEX emp_department_ix;

DROP VIEW emp_details_view;
DROP VIEW wszystko_o_pracowniku

DROP SEQUENCE employees_seq;
DROP SEQUENCE departments_seq;
DROP SEQUENCE locations_seq;

DROP TABLE job_history;
DROP TABLE employees;
DROP TABLE jobs;
DROP TABLE departments;
DROP TABLE locations;
DROP TABLE countries;
DROP TABLE regions;
