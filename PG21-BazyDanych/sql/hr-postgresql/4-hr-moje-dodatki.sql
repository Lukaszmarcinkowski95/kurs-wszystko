CREATE OR REPLACE VIEW szczegoly_pracownika AS
SELECT e.employee_id, e.first_name, e.last_name, e.email, e.phone_number, e.manager_id AS manager_id,
  department_id, d.department_name, d.manager_id AS department_manager_id,
  location_id, l.street_address, l.city, l.postal_code,
  country_id, c.country_name, region_id, r.region_name
FROM employees e
  LEFT JOIN jobs j USING (job_id)
  LEFT JOIN departments d USING (department_id)
  LEFT JOIN locations l USING (location_id)
  LEFT JOIN countries c USING (country_id)
  LEFT JOIN regions r USING (region_id)
ORDER BY e.employee_id;


CREATE OR REPLACE FUNCTION pensje_dzisiaj() RETURNS void AS $$
DECLARE
   pracownik employees%ROWTYPE;
BEGIN
   FOR pracownik IN SELECT * FROM employees
   LOOP
      INSERT INTO salary_history(employee_id, salary, date)
      VALUES (pracownik.employee_id, pracownik.salary, current_date);
   END LOOP;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION przenies_pracownika(
      emp_id employees.employee_id%TYPE,
      dep_id employees.department_id%TYPE,
      job_id employees.job_id%TYPE
   ) RETURNS void AS $$
DECLARE
   stary employees%ROWTYPE;
   data_konca DATE;
BEGIN
   SELECT * FROM employees
      WHERE employee_id = emp_id
      INTO stary;

   IF NOT FOUND THEN
      RAISE EXCEPTION 'employee % not found', emp_id;
   END IF;

   SELECT max(end_date) FROM job_history
      WHERE employee_id = emp_id
      INTO data_konca;
   IF data_konca IS NULL
   THEN data_konca := stary.hire_date;
   END IF;

   INSERT INTO job_history(employee_id, start_date, end_date, job_id, department_id)
   VALUES (emp_id, data_konca, current_date, stary.job_id, stary.department_id);

   UPDATE employees SET
      department_id = dep_id,
      job_id = przenies_pracownika.job_id
   WHERE employee_id = emp_id;
END
$$ LANGUAGE plpgsql;
