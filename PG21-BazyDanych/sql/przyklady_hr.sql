﻿SELECT * FROM employees;
SELECT * FROM jobs;
SELECT * FROM countries;

/* Komentarze w SQL.
   Wersja blokowa */

-- Komentarz jednolinijkowy

-- W poleceniach SQL wielkosc liter nie ma znaczenia
select * from departments;
SeLEct * frOM locations;

-- W nazwach (tabel, kolumn itd.) na pierwszy rzut oka wielkosc liter nie ma znaczenia
SELECT * FROM RegIOns;

-- Jesli wezmiemy nazwe w cudzyslowy, to wtedy wielkosc liter ma znaczenie.
SELECT * FROM "employees"; -- OK
SELECT * FROM "Employees"; -- ZLE
-- Gdy nazwa pisana jest bez "", to baza normalizuje wielkosc liter
-- PostgreSQL zamienia na male litery

CREATE TABLE "Nowa" (wartosc TEXT PRIMARY KEY);
SELECT * FROM Nowa; -- ZLE
SELECT * FROM "Nowa"; -- OK
-- Najlepiej nie uzywac "" od poczatku do konca, w okienkach wpisywac male litery, wtedy wielkosc liter nie znaczenia
-- UWAGA: MySQL pod Linuxem zwraca uwage na wielkosc liter w nazwach, nawet bez cudzyslowow.

/* Uczymy sie SELECT-a */
/* SELECT */
-- Mozna podawac nazwy kolumn
SELECT first_name, last_name FROM employees;
SELECT last_name, first_name, salary FROM employees;

-- Mozna podawac wyrazenia
SELECT first_name, last_name, 12*salary, 2+2 FROM employees;

SELECT first_name || ' ' || last_name, 12*salary, extract('year' FROM hire_date) FROM employees;

-- Dla kolumn można podawać "aliasy", żeby w tabeli wynikowej pojawiły się nasze nazwy kolumn
SELECT first_name, last_name, 12*salary AS roczne FROM employees;

SELECT first_name || ' ' || last_name AS "Kto",
    12*salary AS "Roczne zarobki",
    extract(year FROM hire_date) AS "Rok zatrudnienia"
FROM employees;

-- Uwaga, || do laczenia napisow nie dzialaja w MySQL


SELECT * FROM jobs;
/* Zadanko: tabela jobs, wypisac nazwe stanowiska oraz roznice miedzy maksymalna i minimalna pensja.
   Wersja ciekawsza: policzyc PROCENTOWO jakie sa roznice (wzgledem mniejszej pensji).
*/

SELECT job_title, max_salary - min_salary AS roznica
FROM jobs;
   
SELECT job_title, 100.0 * (max_salary - min_salary) / min_salary AS roznica_procentowa
FROM jobs;


/* WHERE */

SELECT * FROM employees WHERE salary >= 10000;

-- Rownosc to jest pojedyncze =
-- Napisy w SQL umieszcza sie w apostrofach ' '
SELECT * FROM employees WHERE job_id = 'IT_PROG';

-- Rozne - dziala na dwa sposoby
SELECT * FROM employees WHERE job_id <> 'IT_PROG';
SELECT * FROM employees WHERE job_id != 'IT_PROG';

-- AND, oba warunki musza byc spelnione (koniunkcja)
SELECT * FROM employees WHERE job_id = 'IT_PROG' AND salary < 6000;

-- OR, co najmniej jeden z warunkow musi byc spelnione (alternatywa)
SELECT * FROM employees WHERE job_id = 'IT_PROG' OR salary < 6000;

SELECT * FROM employees WHERE salary BETWEEN 5000 AND 10000;

-- rownowazne
SELECT * FROM employees WHERE salary >= 5000 AND salary <= 10000;

-- Wypisz pracownikow z departamentow 40, 50, 60:
SELECT * FROM employees WHERE department_id IN (40, 50, 60);

-- Przyklad zaawansowany (podzapytanie):
-- Wypisz pracownikow pracujacych na stanowiskach, ktorych gorne widelki plac to co najmniej 10tys.
SELECT * FROM employees WHERE job_id IN
    (SELECT job_id FROM jobs WHERE max_salary >= 10000);

/* ORDER BY */
SELECT * FROM employees ORDER BY salary;

-- Mozna podawac nazwy kolumn;
-- Jesli podano wiecej kryteriow, jak tutaj, to osoby sa sortowane wg nazwisk, a o tych samych nazwiskach bede posortowane wg imienia
SELECT * FROM employees ORDER BY last_name, first_name;

-- Mozna sortowac wg wartosci wyrazenia

SELECT * FROM employees ORDER BY length(first_name);

-- Mozna zmienic kolejnosc sortowania: ASC  albo DESC
-- ASC jest domyslne, i raczej sie go nie pisze

-- rosnaco
SELECT * FROM employees
ORDER BY salary;

-- rosnaco
SELECT * FROM employees
ORDER BY salary ASC;

-- malejaco
SELECT * FROM employees
ORDER BY salary DESC;

-- Malejaca wg pensji, ale jak ktos takie same pensje, to rosnaco wg nazwisk
SELECT * FROM employees
ORDER BY salary DESC, last_name;

-- Mozna sortowac po wybranej kolumnie, numeracja od 1. Tu sortowanie wg pensji
SELECT first_name, last_name, salary, job_id
FROM employees
ORDER BY 3;

SELECT first_name, last_name, salary, job_id
FROM employees
ORDER BY 3 DESC, 2, 1;

/* Agregacja danych */

-- Bezposredni odczyt
SELECT salary FROM employees;

-- Zwykla funkcja
SELECT sqrt(salary) FROM employees;

-- Funkcja agregujaca - zamienia cala serie danych w jeden wynik
SELECT avg(salary) FROM employees;

-- Kiedy uzyjemy f.agr., to nie mozemy odwolywac sie do danych indywidualnych
SELECT first_name, last_name, avg(salary) FROM employees;
-- Uwaga - to przejdzie w SQLite, ale nie wiadomo co znaczy wynik
-- Chociaz SELECT first_name, last_name, min(salary) FROM employees
-- ma jakis sens i w SQLite faktycznie zwroci TJ Olson

-- Jest 5 standardowych funkcji agregujacych, a poszczegolne bazy danych dodaja wiele wlasnych rozszerzen

SELECT count(*), sum(salary), avg(salary), min(salary), max(salary)
FROM employees;

-- count moze byc uzywany na 3 sposoby
SELECT department_id FROM employees;

SELECT count(*) AS liczba_rekordow,
    count(department_id) AS ile_nienullowych_wpisow,
    count(DISTINCT department_id) AS ile_roznych_wartosci
FROM employees;

-- Przy okazji
SELECT first_name FROM employees; -- 107
SELECT DISTINCT first_name FROM employees; -- 91 - imiona bez powtorzen
SELECT DISTINCT job_id FROM employees;

/* GROUP BY */
SELECT avg(salary) FROM employees;

SELECT avg(salary) FROM employees GROUP BY job_id;

-- Mozna odwolac sie do kolumn, po ktorych grupujemy, oraz uzywac f.agr.
SELECT job_id, count(*) AS ilu, avg(salary) AS srednia
FROM employees
GROUP BY job_id;

-- wow; ale nie uzywajcie GROUP BY 1, bo to nieprzenosne, na pewno nie dziala w Oraclu
SELECT job_id, count(*) AS ilu, round(avg(salary)) AS srednia
FROM employees
GROUP BY 1
ORDER BY 1;

/* Zadanka... */
-- Policz ilu pracowników zarabia co najmniej 10 tys.
SELECT count(*) FROM employees WHERE salary >= 10000;

-- Wypisz pracowników w kolejności od najdawniej zatrudnionego do najpóźniej zatrudnionego
SELECT * FROM employees ORDER BY hire_date;

-- Wypisz datę najwcześniejszego zatrudnienia oraz datę najpóźniejszego zatrudnienia (same daty, bez danych pracownika).
SELECT min(hire_date), max(hire_date)
FROM employees;

-- Pogrupuj pracowników wg departamentów (department_id) i wypisz liczbę oraz średnią pensję.
SELECT department_id, count(*) AS ilu, avg(salary) AS srednia
FROM employees
GROUP BY department_id;

-- Dolaczamy nazwy departamentow
-- Sposób 1: JOIN
SELECT department_id, department_name, count(*) AS ilu, avg(salary) AS srednia
FROM employees LEFT JOIN departments USING(department_id)
GROUP BY department_id, department_name;

-- Sposób 2: podzapytanie
SELECT department_id,
   (SELECT department_name FROM departments WHERE department_id = employees.department_id)  AS nazwa,
   count(*) AS ilu, avg(salary) AS srednia
FROM employees
GROUP BY department_id;

-- Wypisz ilu pracowników było zatrudnianych w kolejnych latach kalendarzowych
SELECT extract(YEAR FROM hire_date) AS rok,
	count(*) AS ilu,
	avg(salary) AS srednia_pensja
FROM employees
GROUP BY extract(YEAR FROM hire_date)
ORDER BY 1;

-- Uwaga, to nie zadziala w Oraclu
SELECT extract(YEAR FROM hire_date) AS rok,
	count(*) AS ilu,
	round(avg(salary), 2) AS srednia_pensja
FROM employees
GROUP BY rok
ORDER BY rok;


-- Zadanie: Policz ilu pracowników otrzymuje pensje z kolejnych przedziałów po 5 tysięcy: 0-5tys., 5tys - 10 tys., 10 tys - 15 tys. itd.

SELECT first_name || ' ' || last_name AS kto,
	salary,
	round(salary / 1000) AS tysiace_round,
	trunc(salary / 1000) AS tysiace_trunc
FROM employees
ORDER BY salary;

-- podzial wg pensji zaokraglonych do pelnych tysiecy
SELECT round(salary / 1000) AS tysiace, count(*) AS ilu FROM employees
GROUP BY round(salary / 1000);

SELECT count(*) AS ilu,
    'od ' || 5000 * trunc(salary / 5000) || ' do ' || 5000 * (1 + trunc(salary / 5000)) - 1 AS "Przedział"
FROM employees
GROUP BY trunc(salary / 5000)
ORDER BY trunc(salary / 5000);

SELECT count(*) AS ilu,
    'od ' || 5000 * x || ' do ' || 5000 * (1 + x) - 1 AS "Przedział"
FROM (SELECT trunc(salary / 5000) AS x FROM employees) t
GROUP BY x
ORDER BY x;

/* W minimalnym zakresie poruszamy temat transakcji... */
BEGIN TRANSACTION;

UPDATE jobs SET job_title = 'Java Programmer' WHERE job_id = 'IT_PROG';

-- To nie jest tabela, tylko "widok" ("perspektywa") - zdefiniowane zapytanie
SELECT * FROM emp_details_view;

ROLLBACK;  -- anulowanie zmian

COMMIT; -- trwaly zapis zmiana, tak ze staja widoczne dla innych

/* JOIN i zapytania do wielu tabel */

SELECT * FROM employees;
SELECT * FROM departments;
SELECT * FROM locations;
SELECT * FROM countries;
SELECT * FROM regions;
SELECT * FROM jobs;

SELECT (SELECT count(*) FROM employees) * (SELECT count(*) FROM departments);

-- Zapytanie do wielu tabel. Jeśli nie ograniczymy żadnym warunkiem, wychodzi "iloczyn kartezjański", czyli kombinacja "każdego z każdym" (pracownika z departamentem)
SELECT * FROM employees, departments;

-- Alternatywny zapis, znaczenie dokładnie takie samo jak przecinka:
SELECT * FROM employees CROSS JOIN departments;

-- Można dopisać warunek, dzięku któremu dopasujemy pracowników do ICH departamentów
SELECT * FROM employees, departments
WHERE employees.department_id = departments.department_id;

-- Można wprowadzić aliasy tabel - czyli takie lokalne nazwy wewnątrz zapytania,
-- pozwala to skrócić zapis, a czasami (podzapytania) aliasy są potrzebne
SELECT * FROM employees emp, departments dep
WHERE emp.department_id = dep.department_id;

SELECT emp.first_name, emp.last_name, emp.department_id, dep.department_id, dep.department_name
FROM employees emp, departments dep
WHERE emp.department_id = dep.department_id;

-- Kiedy nazwy kolumn nie powtarzaja sie miedzy tabelami, mozna nie uzywac aliasow
SELECT first_name, last_name, emp.department_id, dep.department_id, department_name
FROM employees emp, departments dep
WHERE emp.department_id = dep.department_id;

-- Konstrukcja JOIN: Zamiast pisać przecinek, pisze się JOIN i podaje dodatkowe opcje, np.:
SELECT * FROM employees emp
   JOIN departments dep ON emp.department_id = dep.department_id;

-- Połączymy trzy tabele i dodamy warunek logiczny (salary >= 10tys), aby zobaczyć różnice między podejściem przecinek i WHERE a podejściem JOIN

-- Gdy warunki w WHERE, to mieszamy razem warunki zlaczenia z warunkami filtrowania
SELECT *
FROM employees e, departments d, locations l
WHERE e.department_id = d.department_id
	AND d.location_id = l.location_id
	AND salary >= 10000;

-- Gdy piszemy JOIN, to warunki zlaczenia sa podawane bezposrednio po dolaczeniu kolejnej tabeli
SELECT *
FROM employees e
	JOIN departments d ON e.department_id = d.department_id
	JOIN locations l ON d.location_id = l.location_id
WHERE salary >= 10000;

-- Trzy możliwości podawania warunków złączenia

-- 1) JOIN ON warunek logiczny
SELECT *
FROM employees e
	JOIN departments d ON e.department_id = d.department_id
	JOIN locations l ON d.location_id = l.location_id;

-- 2) JOIN USING(kolumny) - dopasowuje wg wskazanych kolumn
SELECT *
FROM employees
	JOIN departments USING(department_id)
	JOIN locations USING(location_id);

-- 3) NATURAL JOIN - znajduje w tabelach WSZYSTKIE kolumny o takich samych nazwach i dopasowuje wg tych kolumn
SELECT *
FROM employees
	NATURAL JOIN departments
	NATURAL JOIN locations;
-- zwraca tylko tych pracownikow, ktorych szefem jest jednoczesnie szef departamentu
-- jest równoważne:
SELECT *
FROM employees
	JOIN departments USING(department_id, manager_id)
	JOIN locations USING(location_id);


-- Złączenia zewnętrzne i wewnętrzne, różne "kierunki" złączeń
SELECT * FROM employees;
-- 107 rekordów, Kimberely Grant nie ma wpisanego department_id
SELECT * FROM employees WHERE department_id IS NULL;

SELECT * FROM departments;
-- 27 rekordów w tabeli departments
SELECT count(*) FROM departments;
-- ale tylko w 11 faktycznie pracują jacyś pracownicy
SELECT count(distinct department_id) FROM employees;

SELECT * FROM employees JOIN departments USING(department_id);
-- 106 rekordów, tylko pracownicy posiadający deprtment_id (czyli nie ma Kimberely),
-- nie ma departmentów, w których nikt nie pracuje, np. Payroll

-- Domyślnie złączenia są wewnętrzne. Można napisać slowo kluczowe INNER, ale bez niego JOIN działa tak samo.
-- Oba poniższe zapytania są równoważne
SELECT * FROM employees JOIN departments USING(department_id);
SELECT * FROM employees INNER JOIN departments USING(department_id);

-- Są trzy rodzaje złączeń zewnętrznych: LEFT, RIGHT i FULL.
-- Dla każdego z nich można dopisać słowo kluczowe OUTER, które jest tylko ozdobnikiem i nie zmienia działania

SELECT * FROM employees LEFT JOIN departments USING(department_id);
SELECT * FROM employees LEFT OUTER JOIN departments USING(department_id);
-- 107 rekordów, jest Kimberely Grant, dla niej w polach departamentu są wpisane NULL-e

SELECT * FROM employees RIGHT JOIN departments USING(department_id);
SELECT * FROM employees RIGHT OUTER JOIN departments USING(department_id);
-- Nie ma KG, ale są departamenty, w których nikt nie pracuje

SELECT * FROM employees FULL JOIN departments USING(department_id);
SELECT * FROM employees FULL OUTER JOIN departments USING(department_id);
-- Są zarówno KG, jak i departamenty, w których nikt nie pracuje

-- Uwaga: w SQLite nie są dostępne RIGHT JOIN ani FULL JOIN
-- W starszych wersjach baz danych różne produkty obsługiwały rózne wersje składni (czy trzeba pisac OUTER itp.), ale obecnie praktycznie w każdej można pisać tak samo

/* Zadania */

/* Dla pracownika wypisz jego imię, nazwisko i pensję oraz NAZWĘ jego stanowiska (job_title)
 Opcjonalnie: użyj wszystkich 3 sposobów dopasowywania rekordów. */

SELECT first_name, last_name, salary, job_title
FROM employees JOIN jobs USING(job_id);

SELECT first_name, last_name, salary, job_title
FROM employees JOIN jobs ON employees.job_id = jobs.job_id;

SELECT first_name, last_name, salary, job_title
FROM employees NATURAL JOIN jobs;

SELECT first_name, last_name, salary, job_title
FROM employees, jobs
WHERE employees.job_id = jobs.job_id;

/* Dla pracownika w kolejnych kolumnach wypisuj kolejne dane, które powinny się znaleźć na "kopercie" adresowanej do pracownika:
 imię i nazwisko, nazwę departamentu, adres, kod pocztowy, miasto, kraj
 Uwzględnij również Kimberely Grant (niech wypisze się bez danych adresowych). */

SELECT first_name || ' ' || last_name AS "Osoba",
	department_name AS "Departament",
	street_address AS "Ulica",
	postal_code AS "Kod pocztowy",
	city AS "Miasto",
	country_name AS "Kraj"
FROM employees
	LEFT JOIN departments USING(department_id)
	LEFT JOIN locations USING(location_id)
	LEFT JOIN countries USING(country_id);

/* Dla każdego departamentu (również tych, w których nikt nie pracuje) wypisz:
   id, nazwę oraz informację ilu pracowników pracuje i jaka jest ich średnia pensja.
   Posortuj alfabetycznie wg nazwy departamentu.
   Opcja: dopisz jeszcze nazwę miasta, w którym się znajduje departament. */

SELECT department_id, department_name, count(employee_id) AS ilu, round(avg(salary), 1) AS srednia
FROM departments LEFT JOIN employees USING(department_id)
GROUP BY department_id, department_name
ORDER BY 2;

-- Przy okazji: HAVING to jest warunek (jak WHERE) na poziomie zagregowanych danych
-- Wypisuje tylko departamenty, gdzie pracuje więcej niż 1 osoba
SELECT department_id, department_name, count(employee_id) AS ilu, round(avg(salary), 1) AS srednia
FROM departments LEFT JOIN employees USING(department_id)
GROUP BY department_id, department_name
HAVING count(employee_id) > 1
ORDER BY 2;


