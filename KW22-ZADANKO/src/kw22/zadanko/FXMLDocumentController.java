/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kw22.zadanko;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author kurs
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label labelWynik;
  
    
    @FXML
    private void funkcjaDodaj(ActionEvent event) {
        String zLabel = labelWynik.getText();
        int numer = Integer.parseInt(zLabel);
        numer++;
        labelWynik.setText(numer+"");
    }
    @FXML
    private void funkcjaPomnoz(ActionEvent event) {
        String zLabel = labelWynik.getText();
        int numer = Integer.parseInt(zLabel);
        numer=numer*2;
        labelWynik.setText(""+numer);
        
        
    }
    @FXML
    private void funkcjaReset(ActionEvent event) {
        labelWynik.setText("0");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labelWynik.setText("0");
    }    
    
}
