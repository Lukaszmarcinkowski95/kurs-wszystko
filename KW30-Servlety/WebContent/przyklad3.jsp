<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>EJB</title>
</head>
<body>

	<h1>EJB</h1>
	<%-- to jest akcja JSP --%>
	<jsp:useBean id="info" class="pl.alx.InfoBean"></jsp:useBean>

	<%--                          info.getTekst() --%>
	<p>Napis odczytany z beana: ${info.tekst}</p>

	<%-- uruchomi się metoda setTekst() --%>
	<jsp:setProperty property="tekst" name="info" value="Kot ma kompilator"/>
	<p>Napis odczytany z beana: ${info.tekst}</p>
	
	<%-- uruchomi się metoda getAktualnyCzas() --%>
	<p>Aktualny czas: ${info.aktualnyCzas}</p> 
	
	<%-- to nie zadziała, bo nie ma settera do aktualnyCzas w InfoBean --%>
	<%--<jsp:setProperty property="aktualnyCzas" name="info" value="16:06:06"/>  --%>
	
	
	
</body>
</html>













