<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Lista TODO</title>
</head>
<body>

	<h1>Lista TODO</h1>
	
	<h2>Co masz zrobić?</h2>
	
	<form method="post" action="todo-save">
		<input type="text" name="todo_item" placeholder="Pozycja do zapisania" />
		<input type="submit" value="Dodaj" />
	</form>
	
	<hr/>
	
	<h2>Twoja lista TODO</h2>
	
	<c:choose>
		<c:when test="${not empty todo_list}">
			<ul>
				<c:forEach var="todo_item" items="${todo_list}">
					<li>${todo_item}</li>
				</c:forEach>
			</ul>
		</c:when>
		<c:otherwise>
			<p>Strasznie tu pusto! Dodaj cos!</p>
		</c:otherwise>
	</c:choose>
	
	
	
	
</body>
</html>
 













