<%@page import="java.time.LocalTime"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>

	<% int sekunda = LocalTime.now().getSecond(); %>

	<p>Bieżąca sekunda: <%= sekunda %></p>

	<% if (sekunda % 2 == 0) { %>
		<p>sekunda parzysta</p>
	<% } else { %>
		<p>sekunda nieparzysta</p>
	<% } %>
	
	
	<%-- Expression Language (EL) --%>
	<p>Wynik: ${2+3}</p>
	
</body>
</html>

















