<!-- komentarz html -->
<%-- komentarz JSP --%>
<%-- to poniżej to dyrektywa --%>
<%@page import="java.time.LocalTime"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Przykład JSP</title>
	</head>
	<body>
		<h1>Pierwszy plik JSP</h1>
		<p>Ala ma kota</p>
		
		<%-- scriptlet --%>
		|||<%
			System.out.println("Test ze skryptletu");
			out.println("kot ma kompilator");
		%>|||
		

		<%
			String tekst = "Zosia ma psa";
		%>		
		
		<%= tekst %>
		
		<%= "Ola ma " + "konia" %>
		
		
		<p>
			Teraz jest: <%= LocalTime.now() %>
		</p>
	</body>
</html>















