package pl.alx;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Wersaliki")
public class Wersaliki extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private int i=0;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter pr = response.getWriter();
		
		
		pr.println("<html>");
		pr.println("<head>");
		pr.println("<title>Ala ma kota</title>");
		pr.println("</head>");
		pr.println("<body>");
		pr.println("<center>");
		pr.println("<font color = "+'"'+"red"+'"'+">");
		pr.println("<form method ='post' id='formularz'> ");
		pr.println("<label>Podaj tekst </label>");
		pr.println("<textarea name='tekst_zrodlowy'></textarea>");
		pr.println("<input type='submit' />");
		pr.println("</br> i: "+i);
//		pr.println();
//		pr.println();
//		pr.println();
//		pr.println();
		pr.println("</font-color>");
		pr.println("</center>");
		pr.println("</body>");
		pr.println("<html>");
		i++;
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter pr = response.getWriter();
		
		
		pr.println("<html>");
		pr.println("<head>");
		pr.println("<title>Ala ma kota</title>");
		pr.println("</head>");
		pr.println("<body>");
		String tekstZrodlowy = request.getParameter("tekst_zrodlowy");
		
		pr.println("Wpisales: <h1>"+tekstZrodlowy+"</h1>");
		pr.println("</body>");
		pr.println("<html>");

		doGet(request, response);
	}

}
