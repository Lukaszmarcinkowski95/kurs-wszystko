package pl.alx;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/HelloWorld")
public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public HelloWorld() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LocalTime czas = LocalTime.now();
		System.out.println("Zapytanie o godzinie: " + czas);
		
		response.setContentType("text/html");
		PrintWriter pr = response.getWriter();
		pr.println("Czesc! Tu serwlet HelloWorld</br>");
		pr.println("Teraz jest godzina: "+czas+"</br>");
		pr.println("Ala ma <strong>zielonego</strong> kota</br>");
		pr.println("<center>------Koniec Wiadomosci na dzis-------</center></br>");
		
		
		
		
		
	}

}
