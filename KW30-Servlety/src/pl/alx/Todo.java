package pl.alx;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/todo-save")
public class Todo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String todoItem = request.getParameter("todo_item");
		
		if (todoItem != null && !todoItem.isEmpty()) {
			HttpSession sesja = request.getSession();
			
			List todoList = (List) sesja.getAttribute("todo_list");
			
			if (todoList == null) {
				todoList = new LinkedList<>();
				sesja.setAttribute("todo_list", todoList);
			}
			
			todoList.add(todoItem);
		}
		
		response.sendRedirect("todo.jsp");
	}

}












